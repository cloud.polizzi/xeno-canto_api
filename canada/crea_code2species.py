import json
#crea dico code-species
table={}
with open('bird_transcode.txt') as f:
    for line in f:
        parts=line.rstrip().split('\t')
        for code in parts[1:]:
            table[code] = parts[0]
#save
dic=json.dumps(table)
f=open('code2species.json',"w")
f.write(dic)
f.close
