#Import
import urllib
import urllib.request
import json
import numpy as np
import time
import os
secs=60

#Load Files
with open('code2species.json') as f:
    c2sp = json.load(f)
with open('code2species_space.json') as f:
    c2sp_sp = json.load(f)

CODES1=np.loadtxt('birds_codes.txt', dtype=np.str)
CODES2=np.loadtxt('labels_diff4.txt', dtype=np.str)

cntry = "canada"
#Def func
def download_mp3(code,table1,table2,outd):
    species= table1[code]
    res=1
    with urllib.request.urlopen('https://www.xeno-canto.org/api/2/recordings?query={}%20cnt:{}%20q:A'.format(urllib.request.quote(species),urllib.request.quote(cntry))) as url:
        result = json.load(url)

    if result['recordings']==[]:
        print(species+' not found, trying with table2...')
        species= table2[code]

        with urllib.request.urlopen('https://www.xeno-canto.org/api/2/recordings?query={}%20cnt:{}%20q:A'.format(urllib.request.quote(species),urllib.request.quote(cntry))) as url:
            result = json.load(url)

        if result['recordings']==[]:
            print(species+' not found, passing to the next.')
            res=0

    if res==1:

        for npage in np.arange(1,result['numPages']+1,1):

            if npage > 1:
                with urllib.request.urlopen('https://www.xeno-canto.org/api/2/recordings?query={}%20cnt:{}%20q:A&page='.format(urllib.request.quote(species),urllib.request.quote(cntry))+str(npage) ) as url:
                    result = json.load(url)

            recordings=result['recordings']

            for nrec in np.arange(0,len(recordings),1):

                recording=recordings[nrec]
                outputfile = '_'.join((code,recording['type'],recording['q'],recording['id'],recording['date'],recording['time'])).replace(':','').replace('-','').replace('?','').replace(',', '').replace(' ','').replace('/','').replace("'",'').replace('"','').replace('.','')+'.mp3'
                outputfile = os.path.join(outd, outputfile)

                if not os.path.exists(outputfile):

                    with urllib.request.urlopen(recording['url']) as url:
                        page = url.read()

                    if (b"<tr><td>Background</td><td valign='top'>none</td></tr>" in page)==True:
                        t=time.localtime()
                        print('['+str(t[3])+':'+str(t[4])+']: ''Downloading '+outputfile)
                        urllib.request.ubrlretrieve('HTTP:'+recording['file'], outputfile)
                        t=time.localtime()
                        print('['+str(t[3])+':'+str(t[4])+']: '+'File downloaded: '+outputfile)
                        print('~~~~~~~~~60 seconds of sleep~~~~~~~~~')
                        time.sleep(secs)


                else:
                    t=time.localtime()
                    print('['+str(t[3])+':'+str(t[4])+']: '+'ERROR, file already exist: '+outputfile)


outdir1="down"

for code in CODES1:
    try:
        a=download_mp3(code,c2sp,c2sp_sp,outdir1)
    except Exception as e:
        print('Exception for: '+code)
        print(e)
        pass

outdir2="down2"
for code in CODES2:
    try:
        a=download_mp3(code,c2sp,c2sp_sp,outdir2)
    except Exception as e:
        print('Exception for: '+code)
        exep=exep+[code]
        print(e)
        pass
