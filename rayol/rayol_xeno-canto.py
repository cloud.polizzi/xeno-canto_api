#Import
import urllib
import urllib.request
import json
import numpy as np
import time
import os
import pandas as pd
secs = 15



#Def func
def download_mp3(code,outd):
    species = code
    Liste_cnt = pd.DataFrame(columns=['file_name','cnt'])
    res = 1

    with urllib.request.urlopen('https://www.xeno-canto.org/api/2/recordings?query={}%20q:A'.format(urllib.request.quote(species))) as url:
        result = json.load(url)

    if result['recordings']==[]:
        print(species+' not found...')
        res=0


    if res==1:

        for npage in np.arange(1,result['numPages']+1,1):

            if npage > 1:
                with urllib.request.urlopen('https://www.xeno-canto.org/api/2/recordings?query={}%20q:A&page='.format(urllib.request.quote(species))+str(npage) ) as url:
                    result = json.load(url)

            recordings=result['recordings']


            for nrec in np.arange(0,len(recordings),1):

                recording = recordings[nrec]
                outputfile1 = '_'.join((code,recording['type'],recording['q'],recording['id'],recording['date'],recording['time'])).replace(':','').replace('-','').replace('?','').replace(',', '').replace(' ','-').replace('/','').replace("'",'').replace('"','').replace('.','')+'.mp3'
                outputfile = os.path.join(outd, outputfile1)
                tp = pd.DataFrame( { 'file_name': [outputfile1], 'cnt': [recording['cnt']] } )
                Liste_cnt = pd.concat([Liste_cnt,tp], ignore_index = True)

                if not os.path.exists(outputfile):

                    with urllib.request.urlopen(recording['url']) as url:
                        page = url.read()

                    if (b"<tr><td>Background</td><td valign='top'>none</td></tr>" in page) == True:
                        t = time.localtime()
                        print('['+str(t[3])+':'+str(t[4])+']: ''Downloading '+outputfile1)
                        urllib.request.urlretrieve('HTTP:'+recording['file'], outputfile)
                        t = time.localtime()
                        print('['+str(t[3])+':'+str(t[4])+']: '+'File downloaded: '+outputfile1)
                        print('~~~~~~~~~'+str(secs)+' seconds of sleep~~~~~~~~~')
                        time.sleep(secs)

                else:
                    t = time.localtime()
                    print('['+str(t[3])+':'+str(t[4])+']: '+'File already exist: '+outputfile1)

    return Liste_cnt

#Load Files

BIRDS = pd.read_csv('liste_bird.csv')
Liste_cnt = pd.DataFrame(columns=['file_name','cnt'])
exep = []

outdir = "/media/pv/data/Documents/Git_data/GitLab/rayol_indice/Training/downloaded"


for i in BIRDS['taxonomie']:

    try:
        a = download_mp3(i,outdir)
        Liste_cnt = pd.concat([Liste_cnt,a], ignore_index = True)
        Liste_cnt.to_csv('Liste_cnt.csv', header = True, index = False)
    except Exception as e:
        print('Exception for: '+i)
        exep = exep+[i]
        print(e)
        pass
